﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debug_test : MonoBehaviour
{
    private GameObject[] cubes;

    // Start is called before the first frame update
    void Start()
    {
        cubes = new GameObject[2];

        cubes[0] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[0].name = "magic_cube";

        cubes[1] = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cubes[1].name = "cylinder";
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("we have cube object named :" + cubes[0].name, cubes[0]);
        Debug.Log("we have a cylinder object : " + cubes[1].name, cubes[1]);
    }
}
