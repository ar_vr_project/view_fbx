﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem.Sample
{
    public class target_collision_callback : MonoBehaviour
    {
        AudioSource my_audio_source;

        public int scores_earned;

        private readonly int total_score;

        void Start()
        {
            my_audio_source = GetComponent<AudioSource>();
        }


        private void OnCollisionEnter(Collision collision)
        {
            // print out some message to the console
            Debug.Log("target is hit by " + collision.collider, collision.collider);
            Debug.Log("you earned " + scores_earned);

            // play the audio clip
            my_audio_source.Play();
        }

        void Update()
        {
            scores_earned = scores_earned + 1;
            Debug.Log("current total score is >> " + scores_earned);
        }
    }
}
